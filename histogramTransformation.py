from utils import *
from histogram import *

# identical transformation
def identicalTransformation(img, k):
    img = img.astype(np.int64)
    img = img*k
    return eliminateClipping(img)

# inverse transformation
def inverseTransformation(img, k):
    img = img.astype(np.int64)
    maxP = np.max(img)
    img = maxP-img
    img = img*k
    return eliminateClipping(img)

# tresholding transformation
def tresholdingTransformation(img, min, max, treshold):
    img = np.where(img <= treshold, min, max)
    return img.astype(np.uint8)

# linear transformation
def linearTransformation(img, minG=0, maxG=255):
    minP = np.min(img)
    maxP = np.max(img)
    ratioP = maxP - minP
    if ratioP == 0:
        return (img.astype(np.uint8) * 0)
    ratioG = maxG - minG
    ratio = ratioG/ratioP
    img = (img - minP) * ratio
    img = img + minG
    return img.astype(np.uint8)

# specify transformation
def specifyHistogram(img, mappingFunction):
    img = img.astype(np.int64)
    hist = getRelativeHist(img)

    histSum = getHistSum(hist)

    targetHist = np.zeros(hist.shape, dtype=np.float64)
    for i in range(0, 256):
        targetHist[i] = np.abs(mappingFunction(i))

    targetHist /= sum(targetHist)

    targetHistSum = getHistSum(targetHist)

    mapping = np.zeros(hist.shape)
    targetVal = 0
    for i in range(256):
        while targetHistSum[targetVal] < histSum[i]:
            if targetVal >= 255:
                break
            targetVal += 1
        mapping[i] = targetVal

    newImg = transformImageByMapping(img, mapping)
    return newImg, targetHist

# returns histogram sum
def getHistSum(hist):
    histSum = np.zeros(hist.shape)
    histSum[0] = hist[0]
    for i in range(1, 256):
        histSum[i] = histSum[i - 1] + hist[i]

    return histSum

# transform image by histogram mapping
def transformImageByMapping(img, mapping):
    colorImg = len(img.shape) == 3
    newImg = np.zeros(img.shape)
    for r in range(img.shape[0]):
        for c in range(img.shape[1]):
            if colorImg:
                for x in range(img.shape[2]):
                    newImg[r, c, x] = mapping[img[r, c, x]]
            else:
                newImg[r, c] = mapping[img[r, c]]

    return newImg.astype(np.uint8)
