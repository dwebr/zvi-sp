import numpy as np
from histogramTransformation import *

# addition
def pointOperationAddition(img1, img2):
    resultImg = img1.astype(np.int64) + img2.astype(np.int64)
    return linearTransformation(resultImg)

# subtraction
def pointOperationSubtraction(img1, img2):
    resultImg = img1.astype(np.int64) - img2.astype(np.int64)
    return linearTransformation(resultImg)

# multiplication
def pointOperationMultiplication(img1, img2):
    resultImg = img1.astype(np.int64) * img2.astype(np.int64)
    return linearTransformation(resultImg)

# division
def pointOperationDivision(img1, img2):
    resultImg = img1.astype(np.int64) / img2.astype(np.int64)
    return linearTransformation(resultImg)
