import cv2
import numpy as np
from matplotlib import pyplot as plt

# displays RGB histogram of image
def displayHistogramRGB(img):
    plots = []
    for i in range(3):
        hist = cv2.calcHist([img.astype(np.uint8)], [i], None, [256], [0, 256])
        hist /= img.shape[0]*img.shape[1]
        plots.append(hist)
    displayFigure(plots, ['r', 'g', 'b'], 'Histogram RGB')

# displays histogram of image
def displayHistogram(img):
    histSum = getRelativeHist(img)
    displayFigure([histSum], ['b'], 'Histogram')

# displays figure
def displayFigure(plots, colors, title):
    fig = plt.figure()
    fig.canvas.set_window_title(title)
    for i, color in enumerate(colors):
        plot = plots[i]
        plt.plot(plot, color=color)
    plt.xlim([0, 256])
    plt.show()

# returns histogram for all channels
def getAllChannelsHist(img):
    histSum = np.zeros((256, 1))
    channels = 3 if len(img.shape) == 3 else 1
    for i in range(channels):
        hist = cv2.calcHist([img.astype(np.uint8)], [i], None, [256], [0, 256])
        histSum += hist
    hist = np.reshape(hist, (256))
    return histSum

# returns relative histogram for all channels
def getRelativeHist(img):
    colorImg = len(img.shape) == 3
    hist = getAllChannelsHist(img)
    if colorImg:
        hist /= img.shape[0]*img.shape[1]*3
    else:
        hist /= img.shape[0]*img.shape[1]
    return hist
