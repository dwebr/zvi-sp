import cv2
import numpy as np
from tkinter import filedialog
from tkinter import simpledialog
from tkinter import *
import ntpath
import tkinter.messagebox
from matplotlib import pyplot as plt
from sympy import var
from sympy import sympify
from sympy.utilities.lambdify import lambdify

# shows dialog for image input
def getUserImg():
    fileName = filedialog.askopenfilename(initialdir=".", title="Select An Image", filetypes=(
        ("png files", "*.png"), ("jpeg files", "*.jpg"), ("gif files", "*.gif*")))
    if fileName == "":
        return "", ""

    img = cv2.imread(fileName, cv2.IMREAD_COLOR)

    b = img[:, :, 0]
    g = img[:, :, 1]
    r = img[:, :, 2]

    # check if gray scale
    if np.array_equal(r, g) and np.array_equal(g, b):
        img = cv2.imread(fileName, cv2.IMREAD_GRAYSCALE)
    else:
        img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)

    return fileName, img.astype(np.uint8)

# returns file name
def getFileName(path):
    head, tail = ntpath.split(path)
    return tail or ntpath.basename(head)

# saves image
def saveImg(filePath, img):
    name = getFileName(filePath)
    suff = name.rfind(".")
    cv2.imwrite(name[:suff]+"_edited"+name[suff:],
                cv2.cvtColor(img, cv2.COLOR_RGB2BGR))


# loads positive float from user
def loadPositiveFloat(window, title, text):
    answer = simpledialog.askfloat(title, text,
                                   parent=window)
    if answer is None:
        return None
    if answer < 0:
        tkinter.messagebox.showerror(
            title="Error", message="Only positive numbers or zero", parent=window)
        return None

    return answer

# loads int ina range from user
def loadIntInRange(window, title, text, min, max):
    answer = simpledialog.askinteger(title, text,
                                     parent=window)
    if answer is None:
        return None
    if answer < min:
        tkinter.messagebox.showerror(
            title="Error", message="Only numbers greater or equal to "+str(min), parent=window)
        return None

    if answer > max:
        tkinter.messagebox.showerror(
            title="Error", message="Only numbers lower or equal to "+str(max), parent=window)
        return None

    return answer

# loads function from user
def loadFunction(window, title, text):
    userFunctionInput = simpledialog.askstring(title, text,
                                               parent=window)
    if userFunctionInput is None:
        return None
    x = var('x')

    try:
        userFunction = sympify(userFunctionInput)
    except:
        tkinter.messagebox.showerror(
            title="Error", message="Error parsing input", parent=window)
        return None

    # check if more variables
    symbols = userFunction.free_symbols.copy()
    if x in symbols:
        symbols.remove(x)
    if len(symbols) > 0:
        tkinter.messagebox.showerror(
            title="Error", message="Unexpected symbols " + str(symbols) + " in expression: "+str(userFunction), parent=window)
        return None

    return lambdify(x, userFunction)

# return image within range 0 - 255
def eliminateClipping(img):
    img = np.where(img < 0, 0, img)
    img = np.where(img > 255, 255, img)
    return img.astype(np.uint8)
