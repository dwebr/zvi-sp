import cv2
import sys
import numpy as np
from tkinter import *
from tkinter import filedialog
from tkinter import simpledialog
from threading import *
from PIL import Image as Img
from PIL import ImageTk
from utils import *
from pointOperations import *
from histogram import *
from histogramTransformation import *
import tkinter.messagebox

# default size of canvas
WIDTH = 650
HEIGHT = 650

# polling delay when waiting on process
POLLING_DELAY = 250  # ms

# number of back options
IMG_QUEUE_LENGTH = 5

# window definition
window = Tk()
window.title("Histograms")
window.geometry(str(WIDTH)+"x"+str(HEIGHT))
window.resizable(False, False)

# centering window
ws = window.winfo_screenwidth()
hs = window.winfo_screenheight()
x = (ws/2) - (WIDTH/2)
y = (hs/2) - (HEIGHT/2)
window.geometry('%dx%d+%d+%d' % (WIDTH, HEIGHT, x, y))

# global variables definition
imgQueue = []
imgId = -1
fileName = None
isGrayScale = None
resultImg = None
targetHist = None
finished = False

# GUI items
imgLabel = None
menubar = None
filemenu = None
editMenu = None
viewMenu = None
pointOperationMenu = None
brightnessMenu = None

# adds new image at top of image queue
def addImg(img):
    updateImage(img)
    global imgId, imgQueue
    if imgId >= 0:
        imgQueue = imgQueue[:imgId+1]
    imgQueue.append(img)
    if len(imgQueue) > IMG_QUEUE_LENGTH:
        imgQueue.pop(0)
    else:
        imgId += 1
    updateEditButtons()


# disables or enables undo and redo buttons by image queue state
def updateEditButtons():
    if (len(imgQueue)-1) > imgId:
        editMenu.entryconfig("Redo", state=NORMAL)
    else:
        editMenu.entryconfig("Redo", state=DISABLED)

    if imgId > 0:
        editMenu.entryconfig("Undo", state=NORMAL)
    else:
        editMenu.entryconfig("Undo", state=DISABLED)

# undo step
def undo():
    global imgId
    imgId -= 1
    updateImage(imgQueue[imgId])
    updateEditButtons()

# redo step
def redo():
    global imgId
    imgId += 1
    updateImage(imgQueue[imgId])
    updateEditButtons()

# updates GUI image
def updateImage(newImg):
    global imgLabel
    if imgLabel is not None:
        imgLabel.destroy()

    # sets image size
    imgHeight = newImg.shape[0]
    imgWidth = newImg.shape[1]
    ratio = imgWidth / imgHeight
    if ratio > 1:
        labelWidth = WIDTH
        labelHeight = round((labelWidth/imgWidth)*imgHeight)
    else:
        labelHeight = HEIGHT
        labelWidth = round((labelHeight/imgHeight)*imgWidth)
    photoImage = ImageTk.PhotoImage(Img.fromarray(
        newImg).resize((labelWidth, labelHeight)))
    imgLabel = Label(image=photoImage)
    imgLabel.image = photoImage
    imgLabel.place(x=0, y=0)
    window.geometry(str(labelWidth)+"x"+str(labelHeight))
    imgLabel.pack()

# opens user image
def openImage():
    global fileName, imgQueue, imgId, isGrayScale, targetHist, resultImg
    fileNameUsr, img = getUserImg()
    if fileNameUsr == "":
        return
    else:
        fileName = fileNameUsr
    # reset global parameters
    imgQueue = []
    targetHist = None
    resultImg = None
    imgId = -1

    # adds image to queue
    addImg(img)

    # enables menu items
    filemenu.entryconfig("Save", state=NORMAL)
    menubar.entryconfig("Edit", state=NORMAL)
    menubar.entryconfig("Point operations", state=NORMAL)
    menubar.entryconfig("View", state=NORMAL)
    menubar.entryconfig("Brightness scale transformation", state=NORMAL)
    menubar.entryconfig("Equalization", state=NORMAL)
    menubar.entryconfig("Specification", state=NORMAL)

    if len(img.shape) != 3:
        isGrayScale = True
        viewMenu.entryconfig("Histogram RGB", state=DISABLED)
    else:
        viewMenu.entryconfig("Histogram RGB", state=NORMAL)
        isGrayScale = False


# runs image point operation in separate thread
def imagePointOperationActionThread(operationType):
    # user parameters input
    fileName2, img2 = getUserImg()
    if fileName2 == "":
        return
    if img2.shape != imgQueue[0].shape:
        tkinter.messagebox.showerror(
            title="Error", message="Image must have same shape", parent=window)
        return

    global finished
    finished = False

    img = imgQueue[imgId]

    # define and run thread code
    def imagePointOperationAction():
        global resultImg, finished
        if operationType == "addition":
            resultImg = pointOperationAddition(img, img2)
        elif operationType == "subtraction":
            resultImg = pointOperationSubtraction(img, img2)
        elif operationType == "multiplication":
            resultImg = pointOperationMultiplication(img, img2)
        elif operationType == "division":
            resultImg = pointOperationDivision(img, img2)
        finished = True
    Thread(target=imagePointOperationAction).start()

    loadingStart()

    # show image after finish
    def afterFinishAction():
        addImg(resultImg)
    window.after(POLLING_DELAY, lambda: afterFinished(afterFinishAction))

# runs identical transformation in separate thread
def identTransActionThread():
    # user parameters input
    answerK = loadPositiveFloat(
        window, "Identical transformation",  "Select k")
    if answerK is None:
        return

    global finished
    finished = False

    # define and run thread code
    def identTransAction():
        global resultImg, finished
        resultImg = identicalTransformation(imgQueue[imgId], answerK)
        finished = True
    Thread(target=identTransAction).start()

    loadingStart()

    # show image after finish
    def afterFinishAction():
        addImg(resultImg)
    window.after(POLLING_DELAY, lambda: afterFinished(afterFinishAction))

# runs inverse transformation in separate thread
def inversTransActionThread():
    # user parameters input
    answerK = loadPositiveFloat(
        window, "Inverse transformation", "Select k")
    if answerK is None:
        return

    global finished
    finished = False

    # define and run thread code
    def inversTransAction():
        global resultImg, finished
        resultImg = inverseTransformation(imgQueue[imgId], answerK)
        finished = True
    Thread(target=inversTransAction).start()

    loadingStart()

    # show image after finish
    def afterFinishAction():
        addImg(resultImg)
    window.after(POLLING_DELAY, lambda: afterFinished(afterFinishAction))

# runs treshold transformation in separate thread
def tresholdTransActionThread():
    # user parameters input
    answerTreshold = loadIntInRange(
        window, "Thresholding transformation", "Select treshold", 0, 255)
    if answerTreshold is None:
        return
    answerGMin = loadIntInRange(
        window, "Thresholding transformation", "Select g-min", 0, 255)
    if answerGMin is None:
        return
    answerGMax = loadIntInRange(
        window, "Thresholding transformation", "Select g-max", 0, 255)
    if answerGMax is None:
        return
    if answerGMin >= answerGMax:
        tkinter.messagebox.showerror(
            title="Error", message="G-max must be greater than g-min", parent=window)
        return None

    global finished
    finished = False

    # define and run thread code
    def tresholdTransAction():
        global resultImg, finished
        resultImg = tresholdingTransformation(
            imgQueue[imgId], answerGMin, answerGMax, answerTreshold)
        finished = True
    Thread(target=tresholdTransAction).start()

    loadingStart()

    # show image after finish
    def afterFinishAction():
        addImg(resultImg)
    window.after(POLLING_DELAY, lambda: afterFinished(afterFinishAction))


# runs linear transformation in separate thread
def linTransActionThread():
    # user parameters input
    answerGMin = loadIntInRange(
        window, "Thresholding transformation", "Select g-min", 0, 255)
    if answerGMin is None:
        return
    answerGMax = loadIntInRange(
        window, "Thresholding transformation", "Select g-max", 0, 255)
    if answerGMax is None:
        return
    if answerGMin >= answerGMax:
        tkinter.messagebox.showerror(
            title="Error", message="G-max must be greater than g-min", parent=window)
        return None

    global finished
    finished = False

    # define and run thread code
    def linTransAction():
        global resultImg, finished
        resultImg = linearTransformation(
            imgQueue[imgId], answerGMin, answerGMax)
        finished = True
    Thread(target=linTransAction).start()

    loadingStart()

    # show image after finish
    def afterFinishAction():
        addImg(resultImg)
    window.after(POLLING_DELAY, lambda: afterFinished(afterFinishAction))

# runs histogram equalization in separate thread
def equalizeHistogramActionThread():
    global finished
    finished = False

    # define and run thread code
    def equalizeHistogramAction():
        global targetHist, resultImg, finished
        resultImg, targetHist = specifyHistogram(
            imgQueue[imgId], lambda x: 1/255)
        finished = True
    Thread(target=equalizeHistogramAction).start()

    loadingStart()

    # show image and targeting histogram after finish
    def afterFinishAction():
        addImg(resultImg)
        displayFigure([targetHist], ['b'], "Targeting histogram")
    window.after(POLLING_DELAY, lambda: afterFinished(afterFinishAction))


# runs specify histogram in separate thread
def specifyHistogramActionThread():
    # user parameters input
    userFunction = loadFunction(
        window, "Specify function", "function with variable x ")
    if userFunction is None:
        return

    global finished
    finished = False

    # define and run thread code
    def specifyHistogramAction(userFunction):
        global targetHist, resultImg, finished
        resultImg, targetHist = specifyHistogram(imgQueue[imgId], userFunction)
        finished = True
    Thread(target=lambda: specifyHistogramAction(userFunction)).start()

    loadingStart()

    # show image and targeting histogram after finish
    def afterFinishAction():
        addImg(resultImg)
        displayFigure([targetHist], ['b'], "Targeting histogram")
    window.after(POLLING_DELAY, lambda: afterFinished(afterFinishAction))

# puts window into lading state
def loadingStart():
    menubar.entryconfig("File", state=DISABLED)
    menubar.entryconfig("Edit", state=DISABLED)
    menubar.entryconfig("Point operations", state=DISABLED)
    menubar.entryconfig("View", state=DISABLED)
    menubar.entryconfig("Brightness scale transformation", state=DISABLED)
    menubar.entryconfig("Equalization", state=DISABLED)
    menubar.entryconfig("Specification", state=DISABLED)
    loadingLabel = Label(window, text="Loading...")
    loadingLabel.place(x=window.winfo_width()/2-10, y=window.winfo_height()/2)

# removes window into lading state
def loadingEnd():
    menubar.entryconfig("File", state=NORMAL)
    menubar.entryconfig("Edit", state=NORMAL)
    menubar.entryconfig("Point operations", state=NORMAL)
    menubar.entryconfig("View", state=NORMAL)
    menubar.entryconfig("Brightness scale transformation", state=NORMAL)
    menubar.entryconfig("Equalization", state=NORMAL)
    menubar.entryconfig("Specification", state=NORMAL)

# calls callback after variable finished is true
def afterFinished(callback):
    global finished
    if not finished:
        window.after(POLLING_DELAY, lambda: afterFinished(callback))
    else:
        loadingEnd()
        callback()

# defines application menu
def createMenu():
    global filemenu, editMenu, viewMenu, pointOperationMenu, brightnessMenu, menubar
    menubar = Menu(window)
    window.config(menu=menubar)

    # filemenu
    filemenu = Menu(menubar, tearoff=False)
    filemenu.add_command(label="Open", command=openImage)
    filemenu.add_command(
        label="Save", command=lambda: saveImg(fileName, imgQueue[imgId]), state=DISABLED)
    menubar.add_cascade(label="File", menu=filemenu)

    # editMenu
    editMenu = Menu(menubar, tearoff=False)
    editMenu.add_command(
        label="Undo", command=undo, state=DISABLED)
    editMenu.add_command(
        label="Redo", command=redo, state=DISABLED)
    menubar.add_cascade(label="Edit", menu=editMenu, state=DISABLED)

    # viewMenu
    viewMenu = Menu(menubar, tearoff=False)
    viewMenu.add_command(label="Histogram",
                         command=lambda: displayHistogram(imgQueue[imgId]))
    viewMenu.add_command(label="Histogram RGB",
                         command=lambda: displayHistogramRGB(imgQueue[imgId]))
    menubar.add_cascade(label="View", menu=viewMenu, state=DISABLED)

    # pointOperationMenu
    pointOperationMenu = Menu(menubar, tearoff=False)
    pointOperationMenu.add_command(
        label="addition", command=lambda: imagePointOperationActionThread("addition"))
    pointOperationMenu.add_command(
        label="subtraction", command=lambda: imagePointOperationActionThread("subtraction"))
    pointOperationMenu.add_command(
        label="multiplication", command=lambda: imagePointOperationActionThread("multiplication"))
    pointOperationMenu.add_command(
        label="division", command=lambda: imagePointOperationActionThread("division"))
    menubar.add_cascade(label="Point operations",
                        menu=pointOperationMenu, state=DISABLED)

    # brightnessMenu
    brightnessMenu = Menu(menubar, tearoff=False)
    brightnessMenu.add_command(
        label="Identical transformation", command=identTransActionThread)
    brightnessMenu.add_command(
        label="Inverse transformation", command=inversTransActionThread)
    brightnessMenu.add_command(
        label="Thresholding transformation", command=tresholdTransActionThread)
    brightnessMenu.add_command(
        label="Linear transformation",  command=linTransActionThread)
    menubar.add_cascade(label="Brightness scale transformation",
                        menu=brightnessMenu, state=DISABLED)

    # Equalization
    menubar.add_command(
        label="Equalization", command=equalizeHistogramActionThread, state=DISABLED)
    # Specification
    menubar.add_command(
        label="Specification", command=specifyHistogramActionThread, state=DISABLED)


createMenu()
window.mainloop()
